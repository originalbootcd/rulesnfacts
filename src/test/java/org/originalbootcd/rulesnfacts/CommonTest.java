package org.originalbootcd.rulesnfacts;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

public class CommonTest {
	private ByteArrayOutputStream outContent;
	private ByteArrayOutputStream errContent;
	private PrintStream originalOut;
	private PrintStream originalErr;
	
	@BeforeEach
	public void init() {
		outContent = new ByteArrayOutputStream();
		errContent = new ByteArrayOutputStream();
		originalOut = System.out;
		originalErr = System.err;
		System.setOut(new PrintStream(outContent));
		System.setErr(new PrintStream(errContent));
	}
	
	@AfterEach
	public void leave() {
		System.setOut(originalOut);
		System.setErr(originalErr);
	}
	
	@TestFactory
	public Collection<DynamicTest> positiveTestFactory() {
		Collection<DynamicTest> testCollection = new ArrayList<>();
		File positiveInput = new File(getClass().getClassLoader().getResource("positive/input").getFile());
		File positiveOutput = new File(getClass().getClassLoader().getResource("positive/output").getFile());
		for (File inputFile: positiveInput.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".txt");
			}
		})) {
			File outputFile = new File(positiveOutput.getPath() + "/" + inputFile.getName());
			testCollection.add(DynamicTest.dynamicTest(inputFile.getName(), () -> {
				outContent.reset();
				errContent.reset();
				String[] args = {"--deduce", "-itxt", inputFile.getAbsolutePath()};
				Main.main(args);
				String expected = null;
				try (BufferedReader br = new BufferedReader(new FileReader(outputFile))) {
					expected = Optional.ofNullable(br.readLine()).orElse("") + System.getProperty("line.separator");
				}
				catch (IOException e) {
					fail("no output file for test " + inputFile.getName());
				}
				String actual = outContent.toString();
				String errorMessage = "test fail in " + inputFile.getName() + Optional.ofNullable(errContent.toString()).orElse("");
				assertEquals(expected, actual, errorMessage);
			} ));
		}
		return testCollection;
	}
	
	@TestFactory
	public Collection<DynamicTest> negativeTestFactory() {
		Collection<DynamicTest> testCollection = new ArrayList<>();
		File negativeInput = new File(getClass().getClassLoader().getResource("negative/input").getFile());
		for (File inputFile: negativeInput.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".txt");
			}
		})) {
			testCollection.add(DynamicTest.dynamicTest(inputFile.getName(), () -> {
				outContent.reset();
				errContent.reset();
				String[] args = {"--deduce", "-itxt", inputFile.getAbsolutePath()};
				Main.main(args);
				String error = errContent.toString();
				String errorMessage = "test fail in " + inputFile.getName();
				assertFalse(error.isEmpty(), errorMessage);
			} ));
		}
		return testCollection;
	}

	@TestFactory
	public Collection<DynamicTest> positiveTestFactoryXML() {
		Collection<DynamicTest> testCollection = new ArrayList<>();
		File positiveInput = new File(getClass().getClassLoader().getResource("positive/input").getFile());
		File positiveOutput = new File(getClass().getClassLoader().getResource("positive/output").getFile());
		for (File inputFile: positiveInput.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".txt");
			}
		})) {
			File outputFile = new File(positiveOutput.getPath() + "/" + inputFile.getName());
			testCollection.add(DynamicTest.dynamicTest(inputFile.getName(), () -> {
				String[] converterArgs = {"--convert", "-itxt", inputFile.getAbsolutePath(), "-oxml", inputFile.getAbsolutePath() + ".xml"};
				Main.main(converterArgs);
				outContent.reset();
				errContent.reset();
				String[] args = {"--deduce", "-ixml", inputFile.getAbsolutePath() + ".xml"};
				Main.main(args);
				String expected = null;
				try (BufferedReader br = new BufferedReader(new FileReader(outputFile))) {
					expected = Optional.ofNullable(br.readLine()).orElse("") + System.getProperty("line.separator");
				}
				catch (IOException e) {
					fail("no output file for test " + inputFile.getName());
				}
				String actual = outContent.toString();
				String errorMessage = "test fail in " + inputFile.getName() + Optional.ofNullable(errContent.toString()).orElse("");
				assertEquals(expected, actual, errorMessage);
			} ));
		}
		return testCollection;
	}
	
	@TestFactory
	public Collection<DynamicTest> positiveTestFactoryDB() {
		Collection<DynamicTest> testCollection = new ArrayList<>();
		File positiveInput = new File(getClass().getClassLoader().getResource("positive/input").getFile());
		File positiveOutput = new File(getClass().getClassLoader().getResource("positive/output").getFile());
		for (File inputFile: positiveInput.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".txt");
			}
		})) {
			File outputFile = new File(positiveOutput.getPath() + "/" + inputFile.getName());
			testCollection.add(DynamicTest.dynamicTest(inputFile.getName(), () -> {
				String[] converterArgs = {"--convert", "-itxt", inputFile.getAbsolutePath(), "-odb", "mybatis-config2.xml", inputFile.getName()};
				Main.main(converterArgs);
				outContent.reset();
				errContent.reset();
				String[] args = {"--deduce", "-idb", "mybatis-config2.xml", inputFile.getName()};
				Main.main(args);
				String expected = null;
				try (BufferedReader br = new BufferedReader(new FileReader(outputFile))) {
					expected = Optional.ofNullable(br.readLine()).orElse("") + System.getProperty("line.separator");
				}
				catch (IOException e) {
					fail("no output file for test " + inputFile.getName());
				}
				String actual = outContent.toString();
				String errorMessage = "test fail in " + inputFile.getName() + Optional.ofNullable(errContent.toString()).orElse("");
				assertEquals(expected, actual, errorMessage);
			} ));
		}
		return testCollection;
	}
}
