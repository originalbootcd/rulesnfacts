package org.originalbootcd.rulesnfacts;

import java.util.Set;

public class CommandLinePresenter implements Presenter {

	@Override
	public void showError(String errorMsg) {
		System.err.println(errorMsg);
	}

	@Override
	public void showResult(Set<String> knownFacts) {
		System.out.println(String.join(",", knownFacts));
	}

}
