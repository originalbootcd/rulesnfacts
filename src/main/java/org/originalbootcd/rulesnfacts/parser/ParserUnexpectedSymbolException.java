package org.originalbootcd.rulesnfacts.parser;

public class ParserUnexpectedSymbolException extends ParserException {

	public ParserUnexpectedSymbolException(int lineNumber, int index, String state, String ruleString) {
		super(lineNumber, "at " + state + " encountered an unexpected symbol #" + index + " \"" + ruleString.charAt(index) + "\" in " + ruleString);
	}

}
