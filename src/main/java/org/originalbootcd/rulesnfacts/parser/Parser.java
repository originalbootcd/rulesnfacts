package org.originalbootcd.rulesnfacts.parser;

import org.originalbootcd.rulesnfacts.model.Model;

public interface Parser {
	public Model parse() throws Exception;
}
