package org.originalbootcd.rulesnfacts.parser;



import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.originalbootcd.rulesnfacts.data.ExpressionRow;
import org.originalbootcd.rulesnfacts.data.ModelMapper;
import org.originalbootcd.rulesnfacts.data.RuleRow;
import org.originalbootcd.rulesnfacts.model.AndExpression;
import org.originalbootcd.rulesnfacts.model.BooleanExpression;
import org.originalbootcd.rulesnfacts.model.FactExpression;
import org.originalbootcd.rulesnfacts.model.Model;
import org.originalbootcd.rulesnfacts.model.OrExpression;
import org.originalbootcd.rulesnfacts.model.Rule;

public class DbParser implements Parser {
	ModelMapper modelMapper;
	String configFilename;
	String modelName;
	
	public DbParser(String inputFilename, String inputModelName) {
		configFilename = inputFilename;
		modelName = inputModelName;
	}
	
	@Override
	public Model parse() throws Exception {
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(new FileReader(new File(configFilename)));
		Model model = null;
		try (SqlSession session = sqlSessionFactory.openSession()) {
			modelMapper = session.getMapper(ModelMapper.class);
			
			Set<String> knownFacts = modelMapper.getKnownFacts(modelName);
			if (knownFacts.isEmpty())
				throw new Exception("[DbParser] no input known facts");
			
			List<Rule> rules = new LinkedList<>();
			Collection<RuleRow> ruleRows = modelMapper.getRules(modelName);
			for (RuleRow ruleRow: ruleRows) {
				String resultingFact = modelMapper.getFact(ruleRow.resultId);
				ExpressionRow expressionRow = modelMapper.getExpression(ruleRow.exprId);
				BooleanExpression root = getSubtree(expressionRow);
				rules.add(new Rule(root, resultingFact));
			}
			
			model = new Model(rules, knownFacts);
		}
		return model;
	}
	
	
	private BooleanExpression getSubtree(ExpressionRow expressionRow) throws Exception {
		String type = modelMapper.getType(expressionRow.exprType);
		if (type.equals("FACT")) {
			String fact = modelMapper.getFact(expressionRow.factId);
			if (!Pattern.matches("_*\\p{L}[\\p{L}_0-9]*", fact))
				throw new Exception("[DbParser] unexpected fact format " + fact);
				
			return new FactExpression(fact);
		}
		
		List<BooleanExpression> expressionList = new LinkedList<>();
		for (ExpressionRow expressionRowChild: modelMapper.getSubExpressions(expressionRow.id)) {
			expressionList.add(getSubtree(expressionRowChild));
		}
		
		switch (type) {
		case "OR":
			return new OrExpression(expressionList);
		case "AND":
			return new AndExpression(expressionList);
		}
		
		throw new Exception("unexpected expression type #" + expressionRow.exprType + " " + type);
	}

}





















