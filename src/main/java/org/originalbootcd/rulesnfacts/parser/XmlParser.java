package org.originalbootcd.rulesnfacts.parser;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.originalbootcd.rulesnfacts.Main;
import org.originalbootcd.rulesnfacts.model.Model;
import org.xml.sax.SAXException;

public class XmlParser implements Parser {
	private String inputFilename;
	
	public XmlParser(String inputFilename) {
		this.inputFilename = inputFilename;
	}
	
	@Override
	public Model parse() throws Exception {
		Model model = new Model();
		try {
			JAXBContext context = JAXBContext.newInstance(Model.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			unmarshaller.setSchema(getSchema("schema.xsd"));
			model = (Model) unmarshaller.unmarshal(new File(inputFilename));
		}
		catch (JAXBException je) {
			throw new Exception("error parsing xml file " + inputFilename + "; reason: " + je.getMessage());
		}
		return model;
	}
	
    private static Schema getSchema(String schemaResourceName) throws Exception {
        SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
        try {
            return sf.newSchema(Main.class.getClassLoader().getResource(schemaResourceName));
        } catch (SAXException se) {
        	throw new Exception("schema " + schemaResourceName + " format error", se);
        }
        catch (NullPointerException e) {
            throw new Exception("schema " + schemaResourceName + " not found");
        }
    }
}
