package org.originalbootcd.rulesnfacts.parser;

public class ParserException extends Exception {
	static final long serialVersionUID = 102394296L;

	public ParserException(int lineNumber, String arg0) {
		super("[line #" + lineNumber + "] " + arg0);
	}
}
