package org.originalbootcd.rulesnfacts.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.originalbootcd.rulesnfacts.model.AndExpression;
import org.originalbootcd.rulesnfacts.model.BooleanExpression;
import org.originalbootcd.rulesnfacts.model.FactExpression;
import org.originalbootcd.rulesnfacts.model.Model;
import org.originalbootcd.rulesnfacts.model.OrExpression;
import org.originalbootcd.rulesnfacts.model.Rule;

public class TxtParser implements Parser {
	private int lineNumber;
	private int index;
	private enum ParserState {RULES, KNOWNFACTS, END};
	private enum FactParserState {AWAITING_FACT, UNDERSCORED_FACT, FACT, FINAL};
	private enum ExpressionParserState {AWAITING_COMPLEX_FACT, OR, AND, AWAITING_RIGHT_PARENTHESIS, AWAITING_OP};
	
	private String inputFilename;
	
	public TxtParser(String inputFilename) {
		this.inputFilename = inputFilename;
	}
	
	@Override
	public Model parse() throws Exception {
		lineNumber = 0;
		List<Rule> rules = new LinkedList<>();
		Set<String> knownFacts = new TreeSet<>();
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFilename))) {
			String line;
			ParserState state = ParserState.RULES;
			while ((line = bufferedReader.readLine()) != null) {
				lineNumber++;
				line = line.trim();
				if (line.isEmpty())
					continue;
				
				switch (state) {
				case RULES:
					if (line.matches("----------------------------------------------------------------")) {
						state = ParserState.KNOWNFACTS;
						break;
					}
					parseRule(line, rules);
					break;
				
				case KNOWNFACTS:
					parseKnownFacts(line, knownFacts);
					state = ParserState.END;
					break;
					
				case END:
					throw new ParserException(lineNumber, "more than one line expressing facts");
				}
			}
			if (state == ParserState.RULES) {
				throw new ParserException(lineNumber, "delimiter between rules and known facts not found");
			}
			if (state == ParserState.KNOWNFACTS) {
				throw new ParserException(lineNumber, "no known facts");
			}
		}

		return new Model(rules, knownFacts);
	}
	
	private void parseRule(String ruleString, List<Rule> rules) throws ParserException {
		index=0;
		BooleanExpression root = parseExpression(ruleString, true);
		index++;
		if (index >= ruleString.length())
			throw new ParserException(lineNumber, "unexpected end of rule");
		
		if (ruleString.charAt(index) != '>')
			throw new ParserUnexpectedSymbolException(lineNumber, index, "parseRule", ruleString);
		
		index++;
		
		String fact = parseFact(ruleString);
		if (index != ruleString.length())
			throw new ParserUnexpectedSymbolException(lineNumber, index, "parseRule", ruleString);

		rules.add(new Rule(root, fact));
		return;
	}

	private BooleanExpression parseExpression(String ruleString, boolean isInitial) throws ParserException {
		List<BooleanExpression> andExpressions = new LinkedList<>();
		List<BooleanExpression> orExpressions = new LinkedList<>();
		BooleanExpression root = null;
		ExpressionParserState state = ExpressionParserState.AWAITING_COMPLEX_FACT;
		
		for (; index < ruleString.length(); index++) {
			char symbol = ruleString.charAt(index);
			
			switch (state) {
			case AWAITING_COMPLEX_FACT:
				if (Character.isWhitespace(symbol))
					break;
				
				if (symbol == '(') {
					index++;
					root = parseExpression(ruleString, false);
					state = ExpressionParserState.AWAITING_OP;
					break;
				}
				
				root = new FactExpression(parseFact(ruleString));
				state = ExpressionParserState.AWAITING_OP;
				break;
				
			case AWAITING_OP:
				if (Character.isWhitespace(symbol))
					break;
				if (symbol == '|') {
					state = ExpressionParserState.OR;
					break;
				}
				if (symbol == '&') {
					state = ExpressionParserState.AND;
					break;
				}
				if (symbol == ')') {
					if (!isInitial)
						return assembleTree(root, andExpressions, orExpressions);
				}
				if (symbol == '-')
					if (isInitial)
						return assembleTree(root, andExpressions, orExpressions);
				
				throw new ParserUnexpectedSymbolException(lineNumber, index, state.toString(), ruleString);
				
			case OR:
				if (symbol != '|')
					throw new ParserUnexpectedSymbolException(lineNumber, index, state.toString(), ruleString);

				if (andExpressions.isEmpty())
					orExpressions.add(root);
				else {
					andExpressions.add(root);
					orExpressions.add(new AndExpression(andExpressions));
					andExpressions = new LinkedList<>();
				}

				state = ExpressionParserState.AWAITING_COMPLEX_FACT;
				break;
				
			case AND:
				if (symbol == '&') {
					andExpressions.add(root);
					state = ExpressionParserState.AWAITING_COMPLEX_FACT;
					break;
				}
				throw new ParserUnexpectedSymbolException(lineNumber, index, state.toString(), ruleString);
				
			default:
				throw new ParserException(lineNumber, "no case for the state " + state.toString());
			}
		}
		
		throw new ParserException(lineNumber, "unexpected end of line");
	}
	
	private BooleanExpression assembleTree(BooleanExpression root, List<BooleanExpression> andExpressions, List<BooleanExpression> orExpressions) {
		if (!andExpressions.isEmpty()) {
			andExpressions.add(root);
			root = new AndExpression(andExpressions);
		}
		if (!orExpressions.isEmpty()) {
			orExpressions.add(root);
			root = new OrExpression(orExpressions);
		}
		return root;
	}
	
	private String parseFact(String ruleString) throws ParserException {
		StringBuffer factBuffer = new StringBuffer();
		FactParserState state = FactParserState.AWAITING_FACT;
		for (; index < ruleString.length(); index++) {
			char symbol = ruleString.charAt(index);
			
			switch (state) {
			case AWAITING_FACT:
				if (Character.isWhitespace(symbol))
					break;
				
				if (symbol == '_') {
					factBuffer.append(symbol);
					state = FactParserState.UNDERSCORED_FACT;
					break;
				}
				if (Character.isLetter(symbol)) {
					factBuffer.append(symbol);
					state = FactParserState.FACT;
					break;
				}
				throw new ParserUnexpectedSymbolException(lineNumber, index, state.toString(), ruleString);
				
			case UNDERSCORED_FACT:
				if (symbol == '_') {
					factBuffer.append(symbol);
					break;
				}
				if (Character.isLetter(symbol)) {
					factBuffer.append(symbol);
					state = FactParserState.FACT;
					break;
				}
				throw new ParserUnexpectedSymbolException(lineNumber, index, state.toString(), ruleString);
				
			case FACT:
				if (Character.isLetterOrDigit(symbol) || symbol == '_') {
					factBuffer.append(symbol);
					break;
				}
				if (Character.isWhitespace(symbol)) {
					state = FactParserState.FINAL;
					break;
				}
				index--;
				return factBuffer.toString();
				
			case FINAL:
				if (Character.isWhitespace(symbol)) {
					break;
				}
				index--;
				return factBuffer.toString();
			}
		}
		
		if (state != FactParserState.FACT && state != FactParserState.FINAL)
			throw new ParserException(lineNumber, "unexpected end of line");
		
		return factBuffer.toString();
	}
	
	private void parseKnownFacts(String line, Set<String> knownFacts) throws ParserException {
		for (index = 0; index < line.length(); index++) {
			knownFacts.add(parseFact(line));
			if (index == line.length())
				break;
			index++;
			
			if (line.charAt(index) != ',')
				throw new ParserUnexpectedSymbolException(lineNumber, index, "KNOWNFACT", line);
		}
		if (knownFacts.isEmpty())
			throw new ParserException(lineNumber, "no known facts");
	}
	
}













