package org.originalbootcd.rulesnfacts.model;

import java.util.Set;

public interface BooleanExpression extends Serializable {
	public boolean evaluate(Set<String> knownFacts);
}
