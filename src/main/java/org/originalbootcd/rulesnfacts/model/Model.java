package org.originalbootcd.rulesnfacts.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.originalbootcd.rulesnfacts.writer.Serializer;

//@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Model implements Serializable {
	@XmlElementWrapper
	@XmlElement(name = "rule")
	private List<Rule> rules;
	@XmlElementWrapper(name = "knownfacts")
	@XmlElement(name = "fact", required = true)
	private Set<String> knownFacts;
	
	public Model() {
		this.rules = new LinkedList<Rule>();
		this.knownFacts = new TreeSet<String>();
	}
	
	public Model(List<Rule> rules, Set<String> knownFacts) {
		this.rules = rules;
		this.knownFacts = knownFacts;
	}
	
	public Set<String> deduce() {
		int knownFactsSizeBefore = 0;
		do {
			knownFactsSizeBefore = knownFacts.size();
			for (Rule rule: rules)
				rule.deduce(knownFacts);
		} while (knownFacts.size() != knownFactsSizeBefore);
		return knownFacts;
	}
	
	@Override
	public void serialize(Serializer serializer) {
		serializer.serializeModel(rules, knownFacts);
	}
}
