package org.originalbootcd.rulesnfacts.model;

import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

import org.originalbootcd.rulesnfacts.writer.Serializer;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType
public class Rule implements Serializable {
	@XmlElements({
		@XmlElement(name = "and",	type = AndExpression.class),
		@XmlElement(name = "or",	type = OrExpression.class),
		@XmlElement(name = "fact",	type = FactExpression.class)
	})
	private BooleanExpression expression;
	@XmlAttribute(name = "resultingfact")
	private String fact;
	
	public Rule() {
		this.expression = null;
		this.fact = null;
	}

	public Rule(BooleanExpression expression, String fact) {
		this.expression = expression;
		this.fact = fact;
	}
	
	public void deduce(Set<String> knownFacts) {
		if (knownFacts.contains(fact))
			return;
		if (expression.evaluate(knownFacts))
			knownFacts.add(fact);
		return;
	}
	
	@Override
	public void serialize(Serializer serializer) {
		serializer.serializeRule(expression, fact);
	}

}
