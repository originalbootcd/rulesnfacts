package org.originalbootcd.rulesnfacts.model;

import java.util.Set;

import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

import org.originalbootcd.rulesnfacts.writer.Serializer;

@XmlType
public class FactExpression implements BooleanExpression {
	@XmlValue
	private String fact;
	
	public FactExpression() {
		this.fact = "";
	}
	
	public FactExpression(String fact) {
		this.fact = fact;
	}
	
	@Override
	public boolean evaluate(Set<String> knownFacts) {
		return knownFacts.contains(fact);
	}

	@Override
	public void serialize(Serializer serializer) {
		serializer.serializeFactExpression(fact);
	}
	
}
