package org.originalbootcd.rulesnfacts.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

import org.originalbootcd.rulesnfacts.writer.Serializer;

@XmlType
public class AndExpression implements BooleanExpression {
	@XmlElements({
		@XmlElement(name = "and",	type = AndExpression.class),
		@XmlElement(name = "or",	type = OrExpression.class),
		@XmlElement(name = "fact",	type = FactExpression.class)
	})
	private List<BooleanExpression> elements;
	
	public AndExpression() {
		this.elements = new LinkedList<>();
	}
	
	public AndExpression(List<BooleanExpression> elements) {
		this.elements = elements;
	}

	@Override
	public boolean evaluate(Set<String> knownFacts) {
		for (BooleanExpression right: elements) {
			if (!right.evaluate(knownFacts))
				return false;
		}
		return true;
	}

	@Override
	public void serialize(Serializer serializer) {
		serializer.serializeAndExpression(elements);
	}
	
}