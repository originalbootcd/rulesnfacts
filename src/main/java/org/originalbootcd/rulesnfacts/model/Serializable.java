package org.originalbootcd.rulesnfacts.model;

import org.originalbootcd.rulesnfacts.writer.Serializer;

public interface Serializable {
	public void serialize(Serializer serializer);
}
