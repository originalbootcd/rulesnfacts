package org.originalbootcd.rulesnfacts.data;

public class KnownfactRow {
	public int id;
	public int modelId;
	public int factId;

	public KnownfactRow(int modelId, int factId) {
		this.modelId=modelId;
		this.factId=factId;
	}

}
