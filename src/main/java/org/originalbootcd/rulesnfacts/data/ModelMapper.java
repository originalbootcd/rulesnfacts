package org.originalbootcd.rulesnfacts.data;

import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

public interface ModelMapper {
	int									getModel(final String modelName);
	Collection<RuleRow>					getRules(final String modelName);
	TreeSet<String>						getKnownFacts(final String modelName);
	ExpressionRow 						getExpression(final int id);
	Collection<ExpressionRow> 			getSubExpressions(final int id);
	String								getFact(final int id);
	
	int									getFactId(final String fact);
	int									getTypeId(final String type);
	String								getType(final int typeId);
		
	void insertFact			(final FactRow fact);
	void insertExpression	(final ExpressionRow expressionRow);
	void insertKnownFacts	(final List<KnownfactRow> knownfactRow);
	void insertModel		(final ModelRow modelRow);
	void insertRule			(final RuleRow ruleRow);
	
	boolean hasModel		(final String modelName);
	
	void deleteModel(final String modelName);
}
