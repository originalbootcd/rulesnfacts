package org.originalbootcd.rulesnfacts.data;

public class ExpressionRow {
	public int id;
	public Integer parentId;
	public int exprType;
	public Integer factId;
	public int modelId;
	
	public ExpressionRow() {
		factId=null;
	}
	
}
