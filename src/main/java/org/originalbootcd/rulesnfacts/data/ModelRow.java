package org.originalbootcd.rulesnfacts.data;

public class ModelRow {
	public int id;
	public String name;
	
	public ModelRow(String name) {
		this.name = name;
	}
}
