package org.originalbootcd.rulesnfacts;

import org.originalbootcd.rulesnfacts.model.Model;
import org.originalbootcd.rulesnfacts.parser.DbParser;
import org.originalbootcd.rulesnfacts.parser.Parser;
import org.originalbootcd.rulesnfacts.parser.TxtParser;
import org.originalbootcd.rulesnfacts.parser.XmlParser;
import org.originalbootcd.rulesnfacts.writer.DbWriter;
import org.originalbootcd.rulesnfacts.writer.Writer;
import org.originalbootcd.rulesnfacts.writer.XmlWriter;

public class Rulesnfacts {
	private Presenter presenter;
	
	public Rulesnfacts(Presenter presenter) {
		this.presenter = presenter;
	}
	
	public enum InputFormat {TXT, XML, DB};
	public enum OutputFormat {XML, DB};
	
	private Parser createParser(InputFormat inputFormat, String inputFilename, String inputModelName) throws Exception {
		switch (inputFormat) {
		case TXT:
			return new TxtParser(inputFilename);
			
		case XML:
			return new XmlParser(inputFilename);
			
		case DB:
			return new DbParser(inputFilename, inputModelName);
			
		default:
			throw new Exception("unknown input format type");
		}
	}
	
	private Writer createWriter(OutputFormat outputFormat, String outputFilename, String outputModelName) throws Exception {
		switch (outputFormat) {
		case XML:
			return new XmlWriter(outputFilename);
			
		case DB:
			return new DbWriter(outputFilename, outputModelName);
			
		default:
			throw new Exception("unknown output format type");
		}
	}
	
	public void deduce(InputFormat inputFormat, String inputFilename, String modelName) {
		Parser parser = null;
		Model model = null;
		try {
			parser = createParser(inputFormat, inputFilename, modelName);
			model = parser.parse();
		}
		catch (Exception e) {
			presenter.showError(e.toString());
			return;
		}
		presenter.showResult(model.deduce());
	}

	
	public void convert(InputFormat inputFormat, OutputFormat outputFormat, String inputFilename, String inputModelName, 
																String outputFilename, String outputModelName) {
		try {
			Parser parser = createParser(inputFormat, inputFilename, inputModelName);
			Writer writer = createWriter(outputFormat, outputFilename, outputModelName);
			Model model = parser.parse();
			writer.write(model);
		}
		catch (Exception e) {
			presenter.showError(e.toString());
		}
	}
	

}
