package org.originalbootcd.rulesnfacts;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.originalbootcd.rulesnfacts.Rulesnfacts.InputFormat;
import org.originalbootcd.rulesnfacts.Rulesnfacts.OutputFormat;

public class Main {
	public static void main(String[] args) {
		Options options = new Options();
		Option deduce 	= Option.builder("d").longOpt("deduce").desc("deduce from input file").build();
		Option convert 	= Option.builder("c").longOpt("convert").desc("convert from input format to output format").build();
		OptionGroup modeGroup = new OptionGroup();
		modeGroup.addOption(deduce);
		modeGroup.addOption(convert);
		modeGroup.isRequired();
		
		Option txt	= Option.builder("itxt").hasArg().argName("FILE").build();
		Option xml	= Option.builder("ixml").hasArg().argName("FILE").build();
		Option db	= Option.builder("idb").numberOfArgs(2).desc("database read format, where arg is CONFIGFILE MODELNAME").build();
		OptionGroup inputFormatGroup = new OptionGroup();
		inputFormatGroup.addOption(txt);
		inputFormatGroup.addOption(xml);
		inputFormatGroup.addOption(db);
		inputFormatGroup.isRequired();
		
		Option output_txt	= Option.builder("otxt").hasArg().argName("FILE").build();
		Option output_xml	= Option.builder("oxml").hasArg().argName("FILE").build();
		Option output_db	= Option.builder("odb").numberOfArgs(2).build();
		OptionGroup outputFormatGroup = new OptionGroup();
		outputFormatGroup.addOption(output_txt);
		outputFormatGroup.addOption(output_xml);
		outputFormatGroup.addOption(output_db);
		
		
		options.addOptionGroup(modeGroup);
		options.addOptionGroup(inputFormatGroup);
		options.addOptionGroup(outputFormatGroup);
		
		
		
		
		CommandLineParser cli_parser = new DefaultParser();
		CommandLine cli_line = null;
		try {
			cli_line = cli_parser.parse( options, args );
		}
		catch (ParseException e) {
			HelpFormatter hf = new HelpFormatter();
			hf.printHelp("rulesnfacts", options, true);
			e.getMessage();
			return;
		}
		

		Rulesnfacts rulesnfacts = new Rulesnfacts(new CommandLinePresenter());
		InputFormat inputFormat = null;
		OutputFormat outputFormat = null;
		String inputFilename = null;
		String inputModelName = null;
		
		// deduce and convert cases
		if (cli_line.hasOption("itxt")) {
			inputFormat = InputFormat.TXT;
			inputFilename = cli_line.getOptionValue("itxt");
		} else
		if (cli_line.hasOption("ixml")) {
			inputFormat = InputFormat.XML;
			inputFilename = cli_line.getOptionValue("ixml");
		} else
		if (cli_line.hasOption("idb")) {
			inputFormat = InputFormat.DB;
			inputFilename = cli_line.getOptionValues("idb")[0];
			inputModelName = cli_line.getOptionValues("idb")[1];
		}
		
		if (cli_line.hasOption("deduce")) {
			rulesnfacts.deduce(inputFormat, inputFilename, inputModelName);
		} else { 		// cli_line.hasOption("convert"))
			String outputFilename = null;
			String outputModelName = null;
			
			if (cli_line.hasOption("oxml")) {
				outputFormat = OutputFormat.XML;
				outputFilename = cli_line.getOptionValue("oxml");
			} else
			if (cli_line.hasOption("odb")) {
				outputFormat = OutputFormat.DB;
				outputFilename = cli_line.getOptionValues("odb")[0];
				outputModelName = cli_line.getOptionValues("odb")[1];
			}
			rulesnfacts.convert(inputFormat, outputFormat, inputFilename, inputModelName, outputFilename, outputModelName);
		}
	}
}
