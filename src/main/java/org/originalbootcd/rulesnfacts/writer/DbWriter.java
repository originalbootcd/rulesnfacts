package org.originalbootcd.rulesnfacts.writer;

import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.originalbootcd.rulesnfacts.data.ExpressionRow;
import org.originalbootcd.rulesnfacts.data.FactRow;
import org.originalbootcd.rulesnfacts.data.KnownfactRow;
import org.originalbootcd.rulesnfacts.data.ModelMapper;
import org.originalbootcd.rulesnfacts.data.ModelRow;
import org.originalbootcd.rulesnfacts.data.RuleRow;
import org.originalbootcd.rulesnfacts.model.BooleanExpression;
import org.originalbootcd.rulesnfacts.model.Model;
import org.originalbootcd.rulesnfacts.model.Rule;

public class DbWriter implements Writer, Serializer {
	private ModelMapper modelMapper;
	private String configFilename;
	private String modelName;
	private int modelId;
	private Integer currExpressionId;

	public DbWriter(String outputFilename, String outputModelName) {
		this.configFilename	= outputFilename;
		this.modelName		= outputModelName;
	}
	
	@Override
	public void write(Model model) throws Exception {
		SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(new FileReader(new File(configFilename)));
		try (SqlSession session = sqlSessionFactory.openSession()) {
			modelMapper = session.getMapper(ModelMapper.class);
			model.serialize(this);
			session.commit();
		}
	}

	@Override
	public void serializeModel(Collection<Rule> rules, Collection<String> knownFacts) {
		//if (knownFacts.isEmpty()) fail();
		ModelRow modelRow = new ModelRow(modelName);
		if (modelMapper.hasModel(modelName)) {
			modelMapper.deleteModel(modelName);
		}
		modelMapper.insertModel(modelRow);
		modelId = modelRow.id; //modelMapper.getModel(modelName);
		
		for (Rule rule: rules) {
			rule.serialize(this);
		}
		
		List<KnownfactRow> knownfactRows = new LinkedList<>();
		for (String fact: knownFacts) {
			FactRow factRow = new FactRow(fact);
			modelMapper.insertFact(factRow);
			int factId = factRow.id;
			if (factId == 0)
				factId = modelMapper.getFactId(fact);
				
			knownfactRows.add(new KnownfactRow(modelId, factId));
		}
		modelMapper.insertKnownFacts(knownfactRows);
	}

	@Override
	public void serializeRule(BooleanExpression expression, String resultFact) {
		currExpressionId = null;
		expression.serialize(this);
		
		FactRow resultRow = new FactRow(resultFact);
		modelMapper.insertFact(resultRow);
		int resultId = resultRow.id;
		if (resultId == 0)
			resultId = modelMapper.getFactId(resultFact);
		
		RuleRow ruleRow = new RuleRow();
		ruleRow.modelId = modelId;
		ruleRow.resultId = resultId;
		ruleRow.exprId = currExpressionId;
		modelMapper.insertRule(ruleRow);
		
	}

	@Override
	public void serializeFactExpression(String fact) {
		ExpressionRow expressionRow = new ExpressionRow();
		expressionRow.modelId  = modelId;
		expressionRow.parentId = currExpressionId;
		expressionRow.exprType = modelMapper.getTypeId("FACT");
		FactRow factRow = new FactRow(fact);
		modelMapper.insertFact(factRow);
		int factId = factRow.id;
		if (factId == 0)
			factId = modelMapper.getFactId(fact);
		expressionRow.factId = factId;
		modelMapper.insertExpression(expressionRow);
		currExpressionId = expressionRow.id;
	}

	@Override
	public void serializeAndExpression(Collection<BooleanExpression> expressions) {
		ExpressionRow expressionRow = new ExpressionRow();
		expressionRow.modelId  = modelId;
		expressionRow.parentId = currExpressionId;
		expressionRow.exprType = modelMapper.getTypeId("AND");
		modelMapper.insertExpression(expressionRow);
		for (BooleanExpression subexpression: expressions) {
			currExpressionId = expressionRow.id;
			subexpression.serialize(this);
		}
		currExpressionId = expressionRow.id;
	}

	@Override
	public void serializeOrExpression(Collection<BooleanExpression> expressions) {
		ExpressionRow expressionRow = new ExpressionRow();
		expressionRow.modelId  = modelId;
		expressionRow.parentId = currExpressionId;
		expressionRow.exprType = modelMapper.getTypeId("OR");
		modelMapper.insertExpression(expressionRow);
		for (BooleanExpression subexpression: expressions) {
			currExpressionId = expressionRow.id;
			subexpression.serialize(this);
		}
		currExpressionId = expressionRow.id;
		
	}
}
