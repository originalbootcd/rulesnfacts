package org.originalbootcd.rulesnfacts.writer;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.originalbootcd.rulesnfacts.model.Model;

public class XmlWriter implements Writer {
	String outputFilename;

	public XmlWriter(String outputFilename) {
		this.outputFilename = outputFilename;
	}
	
	@Override
	public void write(Model model) throws Exception {
		JAXBContext context = JAXBContext.newInstance(Model.class);
		Marshaller m = context.createMarshaller();
		m.marshal(model, new File(outputFilename));
	}

}
