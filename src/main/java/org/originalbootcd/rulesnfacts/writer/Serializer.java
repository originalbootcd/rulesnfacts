package org.originalbootcd.rulesnfacts.writer;

import java.util.Collection;

import org.originalbootcd.rulesnfacts.model.BooleanExpression;
import org.originalbootcd.rulesnfacts.model.Rule;

public interface Serializer {
	void serializeModel(Collection<Rule> rules, Collection<String> knownFacts);
	void serializeRule(BooleanExpression expression, String resultFact);
	void serializeFactExpression(String fact);
	void serializeAndExpression(Collection<BooleanExpression> expressions);
	void serializeOrExpression(Collection<BooleanExpression> expressions);
}
