package org.originalbootcd.rulesnfacts;

import java.util.Set;

public interface Presenter {
	public void showError(String errorMsg);
	public void showResult(Set<String> knownFacts);
}
